from enum import Enum


class command_message_type(Enum):
    Up = 1
    Down = 2

class command_message:

    def __init__(self, command=None, data=None):
        self.command = command
        self.data = data

    def __str__(self):
        return('command_message(command={0}, data={1})'.format(str(self.command), str(self.data)))

class response_message:
    def __init__(self, command=None, success=None):
        self.command = command
        self.success = success

    def __str__(self):
        return('response_message(command={0}, success={1})'.format(str(self.command), str(self.success)))
