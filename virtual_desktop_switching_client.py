#!/usr/bin/env python3
import zmq
import logging

from messages import command_message, command_message_type

class DesktopClient:
    """A class to interact with desktop_server to control switching of virtual desktops"""

    def __init__(self, zmq_host='localhost', zmq_port=18334):
        """
        Constructor

        :param string zmq_host: The hostname or IP address to connect to
        :param int zmq_port: ZMQ port to connect on

        :return desktop_client object
        :rtype desktop_client
        """
        self.zmq_host = zmq_host
        self.zmq_port = zmq_port

        self.connection_url = None
        self.context = None
        self.zmq_socket = None

    def connect(self):
        """
        Connects to the ZMQ server

        :return: void
        """
        self.connection_url = "tcp://{0}:{1:d}".format(self.zmq_host, self.zmq_port)
        self.context = zmq.Context()
        logging.info('Connecting to server: {0}'.format(self.connection_url))
        self.zmq_socket = self.context.socket(zmq.REQ)
        self.zmq_socket.connect(self.connection_url)
        logging.info('Connected to server: {0}'.format(self.connection_url))

    def disconnect(self):
        """
        Disconnects from the ZMQ server

        :return: void
        """
        self.zmq_socket.close()

    def switch(self, command_message_obj):
        """
        Sends command to switch the host display

        :param command_message_obj command_message_obj: The command message to send

        :return: void
        """

        # Convert str to command_message if necessary and check type
        if type(command_message_obj) is str:
            logging.info('type(command_message) is str - command_message == {0}'.format(command_message_obj))
            if command_message_obj.lower() == 'up':
                command_message_obj = command_message(command=command_message_type.Up)
            elif command_message_obj.lower() == 'down':
                command_message_obj = command_message(command=command_message_type.Down)
            else:
                logging.error('If type(command_message) is str then expecting it to be up or down')
        elif type(command_message_obj) is not command_message:
            logging.error('Expecting type(command_message) to be str or command_message')


        logging.info('Sending command message: {0}'.format(command_message_obj))
        self.zmq_socket.send_pyobj(command_message_obj)

        response_message = self.zmq_socket.recv_pyobj()
        logging.info('Received response message: {0}'.format(response_message))


if __name__ == "__main__":
    desktop_client = DesktopClient(zmq_host='localhost', zmq_port=18334)
    desktop_client.connect()
    #desktop_client.switch(command_message(command=command_message_type.Down))
    #desktop_client.switch('Up')
    desktop_client.disconnect()

