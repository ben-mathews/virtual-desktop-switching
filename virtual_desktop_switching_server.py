#!/usr/bin/env python3
import zmq
import time
import sys
import os
#from xdo import Xdo #Tried using this, but did not get consistent behavior
import logging

from messages import command_message, response_message, command_message_type

class DesktopServer:
    """
    A class to handle virtual desktop switching
    """

    def __init__(self, zmq_host='localhost', zmq_port=18334):
        """
        Constructor

        :param string zmq_host: The hostname or IP address to connect to
        :param int zmq_port: ZMQ port to connect on

        :return desktop_client object
        :rtype desktop_client
        """
        self.zmq_host = zmq_host
        self.zmq_port = zmq_port

        self.server_url = None
        self.context = None
        self.zmq_socket = None

    def connect(self):
        """
        Opens the ZMQ server socket

        :return: void
        """
        # Start up XZM server
        self.server_url = "tcp://*:{0:d}".format(self.zmq_port)
        self.context = zmq.Context()
        self.zmq_socket = self.context.socket(zmq.REP)
        logging.info('Starting server on: {0}'.format(self.server_url))
        self.zmq_socket.bind(self.server_url)
        logging.info('Server started on: {0}'.format(self.server_url))

    def run(self):
        """
        Runs the virtual desktop switching server main loop

        :return: void
        """
        logging.info('Entering main processing loop')
        while True:
            logging.info('Listening for new command message')
            command_message_obj = self.zmq_socket.recv_pyobj()
            logging.info('Received command message: {0}'.format(command_message_obj))
            if command_message_obj.command == command_message_type.Up:
                response_message_obj = response_message(command=command_message_obj.command, success=True)
                # xdo.set_current_desktop(1)
                os.system('xdotool set_desktop --relative -- -1')
            elif command_message_obj.command == command_message_type.Down:
                response_message_obj = response_message(command=command_message_obj.command, success=True)
                os.system('xdotool set_desktop --relative 1')
            else:
                response_message_obj = response_message(command=command_message_obj.command, success=False)
            logging.info('Sending response message: {0}'.format(response_message_obj))
            self.zmq_socket.send_pyobj(response_message_obj)


if __name__ == "__main__":
    desktop_server = DesktopServer(zmq_host='localhost', zmq_port=18334)
    desktop_server.connect()
    desktop_server.run()


