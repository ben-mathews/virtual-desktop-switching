#!/usr/bin/env python3
"""  Script for calling and using desktop_client.py to switch desktops
Usage:
    desktop_switcher.py [server_ip] [server_port] [command]

Parameters:
    server_ip       IP address of the desktop switching server
    server_port     TCP port number of the desktop switching server
    command         "Up" or "Down"

Example:

    desktop_switcher.py 127.0.0.1 18334 Down
"""

import sys
from desktop_client import DesktopClient

# TODO: Check input args
server_ip = "192.168.122.1"
server_port = 18334
server_command = "Up"

desktop_client = DesktopClient(zmq_host=server_ip, zmq_port=server_port)
desktop_client.connect()
desktop_client.switch(server_command)
desktop_client.disconnect()
